# Joystcik Driver for ubuntu

Custom joystick driver for ubuntu. 

TODO: testing with Python 3

## Description:
This ros package is for connecting the joystick with linux system.
The main script is joystickCmd.py. It will invoke the nameread.py.
nameread.py invoke some func in ridraw.
Now the script is tested with python2.7. For python3, the decode part should be changed.

## Parameters: 

The actual position is published in topic joystickPosition. (the range is from -1 to 1)
The target position should be sand in topic target_pos_joy. But you can also use rosparam to change the name of the topic as you want.(the range is from -1 to 1)
Amp of the force can be changed in func change_force(x,y) the followed lines:(this 4000 is amp)

spring damper model
```
#4000 can be changed
amp_1 = -1* pos_del_x4000
amp_2 = -1 pos_del_y*4000
When you want to change other thing, please turn to the CANopen manuel.
```

## HowTO:

Preparation:
please first run the followed cmd for every hidraw device to let the port be readable and writeable.
```
sudo chmod 777 /dev/hidrawX
```
Then make sure which port is for the joystick, e.g. hidraw2 and hidraw3.
(To do this, you can just pluck all the USB device from your computer, only plug the joystick on it. And use ls /dev/ to check the name start with hidraw.)
Only run the followed cmd in nameread.py to get the RawReport code of the two hidraw ports.
```
code = getFile.getRawReportDescriptor().encode("hex")
print(code)
```
Copy and past the two code in RawReport1 and RawReport0.
After all of this, next time when you start your computer and plug other usb device, the scripts can still find the right port automatlly.
But next time you still need to chmod for all the hidraw device.


Run the script:
Please use the launch file in launch folder,  which will start the main script.
After that, the joystick will start to initialize at first. And then move to the center position. Now the position is published with 100Hz in joystickPosition topic with message PointStamped.
The point.x is the actual position of short axis, point.y is the actual position of long axis.


Control the position of the joystick:
please send the PointStamped msg in topic target_pos_joy. (x,y is the position of the 2axis, range from -1 to 1)
The simulation environment for the demonstrator. Stage 1 is to have a simple testbench for guest and visitors until 2019 FZI Open House.