#!/usr/bin/env python
import rospy
from geometry_msgs.msg import PointStamped, Point
import time
import math
#import threading
#import logging

def talker():
    pub = rospy.Publisher('/joy/target_pos',Point,queue_size = 1)
    rospy.init_node('joystickDemo', anonymous=True)
    rospy.loginfo("in 3sec start the motion")
    time.sleep(3)
    count = 0
    rate = rospy.Rate(50)
    data = Point()
    while not rospy.is_shutdown():
        # data = PointStamped()
        #data.point.x = 0.0
        #data.point.y = 0.0

        data.x = 0.8*math.cos(count)
        data.y = 0.8*math.sin(count)
        pub.publish(data)
        rate.sleep()
        count = count + (3.1415)/200
        if(count>=2*3.1415):
            count=0
    rospy.spin()

if __name__ == "__main__":
    talker()
