#!/usr/bin/env python3
import rospy
from std_msgs.msg import Int32,Float32
from geometry_msgs.msg import PointStamped, Point
import struct
import time
import codecs  
from nameread import check_port
from threading import Thread, Lock
import os
import numpy as np



global tar_x
global tar_y
global act_x
global act_y
global tar_force_x
global tar_force_y
tar_x=0.0
tar_y=0.0
tar_force_x = 0.0
tar_force_y = 0.0
act_x = 0.0
act_y = 0.0
hid0, hid1 = check_port()
fw=open(hid1,"rb+")


def data_makeup(node=1,obFrame=0x1000,subFrame=0x00,frame=0x2b,dataFrame=0x0000):
    startFrame = 0x113f
    countFrame = 0x0000
    canidFrame = 0x8600 + node      #600 SDO,read/write
    csFrame = frame                 #write 1,2,3,4 Bytes:2f 2b 27 23(60 successfully write)  read 1,2,3,4 Bytes:4f 4b 47 43 40(read) 
    byteData = struct.pack('<3HBHBl6QH',startFrame,countFrame,canidFrame,csFrame,obFrame,subFrame,dataFrame,0,0,0,0,0,0,0)
    #print(byteData)
    # sendData = byteData.decode('utf-16') #codecs.decode(byteData, 'UTF-8')  
    return byteData # sendData


def change_force(x,y):
    # read velocity
    #vel_1 = read_vel(1)
    #vel_2 = read_vel(2)
    # control position with spring substitution system
    global tar_x
    global tar_y

    # force control directly
    global tar_force_x
    global tar_force_y

    # calculate difference of position
    pos_del_x = float(tar_x-x)  
    pos_del_y = float(tar_y-y)
    '''
    print("delta x is %f" % pos_del_x)
    print("delta y is %f" % pos_del_y)
    print("act x is %f" % act_x)
    print("act y is %f" % act_y)
    print("tar x is %f" % tar_x)
    print("tar y is %f" % tar_y) 
    '''
    controller_type = rospy.get_param('/joy/controller_type')
    spring_constant = rospy.get_param('/joy/spring_constant')
    # Init default forces
    amp_1 = 0
    amp_2 = 0
    if controller_type == 'pos':
        # spring damper model
        #4000 can be changed
        amp_1 = -1* np.arctan(2*pos_del_x)*spring_constant*np.sqrt(np.abs(pos_del_x))
        amp_2 = -1* np.arctan(2*pos_del_y)*spring_constant*np.sqrt(np.abs(pos_del_y))

    elif controller_type == 'force':
        # direct force control with limit
        if abs(tar_force_x) < 4000:
            amp_1 = tar_force_x
        if abs(tar_force_y) < 4000:
            amp_2 = tar_force_y
        #
        
    else:
        # Error on the ros parameter
        amp_1 = 0
        amp_2 = 0
    

    if amp_1 < 0:
        amp_1 = 65535 + amp_1 + 1
    if amp_2 < 0:
        amp_2 = 65535 + amp_2 + 1

    # get force and send
    #if abs(x)<1.5 and abs(y) < 1.5:
    send_force(1,int(amp_1))
    send_force(2,int(amp_2))


def send_force(node,amp):
    global fw
    fw.write(data_makeup(node,0x6071,0x00,0x2b,amp))
    back = fw.read(64)
    


def read_pos():
    global fw
    pos_1 = None
    pos_2 = None
    fw.write(data_makeup(1,obFrame=0x6063,subFrame=0x00,frame=0x40,dataFrame=0x0000))
    a1,a2,a3,a4,a5,a6,a7,_,_,_,_,_,_,_=struct.unpack('<3HBHBl6QH',fw.read(64))
    if(a5==(0x6063)):
        pos_1 = max(-1.0,min(float(a7/2000.0),1.0))  #should be changed according to real range 
    else:
        rospy.loginfo("cannot get velocity")
        pos_1 = 0.0
    fw.write(data_makeup(2,obFrame=0x6063,subFrame=0x00,frame=0x40,dataFrame=0x0000))
    a1,a2,a3,a4,a5,a6,a7,_,_,_,_,_,_,_=struct.unpack('<3HBHBl6QH',fw.read(64))
    if(a5==(0x6063)):
        pos_2 = max(-1.0,min(float(a7/2000.0),1.0)) #should be changed according to real range 
    else:
        rospy.loginfo("cannot get velocity")
        pos_2 = 0.0
    return (pos_1, pos_2)

def target_force_callback(targetForce):
    global tar_force_x
    global tar_force_y
    # Changing coordinats Brunner has different x-y convention  
    # --> to have a consisten x-y cord-system with the vehicle in the test-bench
    # Focres are in the other dierction defined
    tar_force_x = -targetForce.y
    tar_force_y = -targetForce.x

def target_pos_callback(targetPose):
    global fw
    global tar_x
    global tar_y
    global act_x
    global act_y
    
    # Changing coordinats Brunner has different x-y convention  
    # --> to have a consisten x-y cord-system with the vehicle in the test-bench
    tar_x = targetPose.y
    tar_y = targetPose.x

    #calculate the force
    #change_force(act_x, act_y)


def init():
    time.sleep(1)
    print("start initialization!")
    cmd=[]
    cmd.append(data_makeup(1,0x2101,0x01,0x2b,0x0602))
    cmd.append(data_makeup(1,0x6040,0x00,0x2b,0x007e))
    cmd.append(data_makeup(1,0x6040,0x00,0x2b,0x00fe))
    cmd.append(data_makeup(1,0x6040,0x00,0x2b,0x007f))
    cmd.append(data_makeup(1,0x6040,0x00,0x2b,0x007f))
    cmd.append(data_makeup(1,0x2101,0x01,0x2b,0x2602))

    cmd.append(data_makeup(2,0x2101,0x01,0x2b,0x0602))
    cmd.append(data_makeup(2,0x6040,0x00,0x2b,0x006e))
    cmd.append(data_makeup(2,0x6040,0x00,0x2b,0x00ee))
    cmd.append(data_makeup(2,0x6040,0x00,0x2b,0x006f))
    cmd.append(data_makeup(2,0x6040,0x00,0x2b,0x006f))
    cmd.append(data_makeup(2,0x2101,0x01,0x2b,0x2602))
    for i in range(len(cmd)):
        #data2sendbyte = cmd[i].encode('UTF-16')
        fw.write(cmd[i])
        fw.read(64)
        time.sleep(0.005)
    time.sleep(1.0)
    print("initialization finished!")


def listener():
    rospy.init_node('joystickForce',anonymous=True)
    rospy.Subscriber('/joy/target_pos', Point, target_pos_callback)
    rospy.Subscriber('/joy/target_force', Point, target_force_callback)
    pub = rospy.Publisher('/joy/actual_pos',Point, queue_size = 1)
    mutex = Lock()
    r = rospy.Rate(100)
    data = Point()
    while not rospy.is_shutdown():
        #mutex.aquire()
        act_x, act_y = read_pos()
        change_force(act_x, act_y)
        # Changing coordinats Brunner has different x-y convention  
        # --> to have a consisten x-y cord-system with the vehicle in the test-bench
        data.y = act_x
        data.x = act_y
        pub.publish(data)
        #mutex.release()
        #rospy.spinOnce()
        r.sleep()


if __name__ == "__main__":
    init()
    listener()
