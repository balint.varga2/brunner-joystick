import os, sys
from hidraw import HIDRaw 

def check_port():
    path = "/dev/"
    dirs = os.listdir(path)
    container = [0,0]
    #RawReport1 = "0600ff0a02ffa101853f953f7508150025ff0a01ff81020a01ff9102c0"
    #RawReport0 = "05010904a101050919012905150025013600004601007501950581027503950181030501093915012508360000463b0166140055007504950181427504950181030501150026ffff36000046ffff650009307510950181020931751095018102c0"
    
    # Balint PC
    RawReport1 = "0600c3bf0a02c3bfc2a101c2853fc2953f7508150025c3bf0a01c3bfc281020a01c3bfc29102c380"
    RawReport0 = "05010904c2a101050919012905150025013600004601007501c29505c281027503c29501c281030501093915012508360000463b0166140055007504c29501c281427504c29501c281030501150026c3bfc3bf36000046c3bfc3bf650009307510c29501c2810209317510c29501c28102c380"
    
    for file in dirs:
        if "hidraw" in file:
            path = "/dev/"+file
            #print(path)
            fr=open(path,"r")
            getFile = HIDRaw(fr)
            # test this part with python 3 --> done, it works
            code = getFile.getRawReportDescriptor().encode("UTF-8").hex()
            #print(code)

            if code == RawReport1:
                #print("dev1 is: "+path)
                container[1] = path
            elif code == RawReport0:
                #print("dev0 is: "+path)
                container[0] = path
    for idx,name in enumerate(container):
        if(name==0):
            print("port %d is not exist!" % idx)

    return (container[0], container[1])
        
#check_port()

